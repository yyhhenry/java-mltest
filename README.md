# JavaMLTest

测试JavaML框架

前端采用Electron，如果从Gitee下载，请自行在javaml-electron文件夹中运行`yarn app:build`得到可执行文件。

如果仅仅测试java部分，可以采用scripts中的web页预览可视化效果，但是操作繁琐效果较差。

## 源码描述


1. Java部分（底层）
enterpoint.Main 入口类（调用JavaML库中KDtreeKNN进行分类）
mymath.MyMatrix 矩阵类（用于QR分解和QR迭代求矩阵的特征值和特征向量）
mymath.MyPCA 主成分分析类（用于求高维数据的主成分，方便在二维平面上可视化，展示分类效果）
2. TypeScript+Vue部分（前端）
`./javaml-electron/`，采用Electron(Node.js)+Vue.js+TypeScript的方式建立前端页面，是本项目正常的前端界面，在build之后可以使用根目录下`run.cmd`打开（仅Windows，如果使用其它系统，可以修改Electron的配置文件同样可以打包成对应的本地应用）。
3. HTML（替代性前端）
 `./scripts/`为了便于显示，此处采用了一个不需要Electron的开发工具也能直接显示的前端，用html+js可以在本地打开。


## 资源描述

1. `./lib/*.jar`使用JavaML库所必须的jar库
2. `./resource/*`分类使用的数据，来自[https://github.com/ricardovvargas/3w_dataset](https://github.com/ricardovvargas/3w_dataset)但是经过筛选和基本的文本调整（这一部分脚本使用ts-node）。

## LICENSE

开源许可: 木兰宽松许可证， 第2版

可以复制、使用、修改、分发其“贡献”，不论修改与否。


## 运行时依赖

完成Electron的Build之后不再依赖TypeScript/Vue/Electron的任何工具包，但是需要能够正常运行的JavaMLTest.jar文件放在根目录下，请勿删除。

临时文件：
1. plain.csv用来描述PCA逆向遍历高维空间的点集。
2. points.javaml-point.txt用来向前端传递渲染所必须的结果
3. output-log.txt用来描述分类器在测试数据集上的表现，和运行时数学库的调试输出。

.gitignore规定了这些文件不会上传到Gitee。
