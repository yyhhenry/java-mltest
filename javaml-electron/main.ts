import { app, BrowserWindow, Menu } from 'electron';
import * as path from 'path';

console.log(process.env.MODE);
const entryURL = process.env.MODE === 'development'
    ? 'http://localhost:3000'
    : path.join(__dirname, '../src/index.html');
const createWindow = () => {
    if (process.env.MODE !== 'development') {
        Menu.setApplicationMenu(null);
    }
    const win = new BrowserWindow({
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
        }
    });
    win.setSize(800, 600);
    const setContentSize = () => {
        const [width, height] = win.getSize();
        win.setContentSize(width, height);
    };
    setContentSize();
    win.addListener('resized', setContentSize);
    win.loadURL(entryURL);
};
app.whenReady().then(() => {
    createWindow();
    app.on('activate', () => {
        if (BrowserWindow.getAllWindows().length === 0) {
            createWindow();
        }
    });
});
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});
